-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: ocm
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `a_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `is_active` int NOT NULL DEFAULT '0',
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT 'admin',
  PRIMARY KEY (`a_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'akshay','akshay',0,'patil','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','admin'),(2,'akshayfegade15@gmail.com','Akshay',0,'Bharambe','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','admin'),(3,'akshay1','qqqqqqq',0,'patil','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','admin'),(4,'akshay@gmail.com','Akshay',0,'fegade','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','admin'),(5,'akshay@gmail.com','akshay',0,'fegade','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','admin'),(6,'ashwin@gmail.com','Ashwin',0,'Bharambe','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','admin'),(7,'akshay@gmail.com','Akshay',0,'Patil','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','admin'),(8,'gokul@gmail.com','Gokul',0,'Mahajan','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','admin'),(9,'nitesh@gmail.com','Nitesh',0,'Chaudhary','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','admin'),(10,'vaibhav@gmail.com','Vaibhav',0,'Dhake','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','admin');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_content`
--

DROP TABLE IF EXISTS `course_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course_content` (
  `course_id` int NOT NULL AUTO_INCREMENT,
  `course_fee` double DEFAULT NULL,
  `course_title` varchar(255) DEFAULT NULL,
  `createdtimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `duration` varchar(255) DEFAULT NULL,
  `prerequisite` text,
  `syallbus` text,
  `tags` varchar(255) DEFAULT NULL,
  `l_id` int DEFAULT NULL,
  PRIMARY KEY (`course_id`),
  KEY `FK1k4j1xvxqdx6tjhwpyvyw2wx4` (`l_id`),
  CONSTRAINT `FK1k4j1xvxqdx6tjhwpyvyw2wx4` FOREIGN KEY (`l_id`) REFERENCES `lecturer` (`l_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_content`
--

LOCK TABLES `course_content` WRITE;
/*!40000 ALTER TABLE `course_content` DISABLE KEYS */;
INSERT INTO `course_content` VALUES (3,5000,'Core Java','2022-01-29 21:50:52','98','not require','xyz','java',1),(4,8000,'Advance Java','2022-01-29 21:52:35','98','core java','xyz','java',1),(11,9000,' java OOPS ','2022-03-24 18:37:44','9','any core language like java,c','\nIntroduction to Java and OOPS\nJava Tokens- Comments, Identifiers, Keywords, Separators\nWorking with Java Editor Softwares - Editplus, NetBeans, Eclipse\nPackages with static imports\nWorking with jar\nModifiers - File level, Access level and Non-access level\nDatatypes, Literals, Variables, Type Conversion, Casting & Promotion\nReading runtime values from keyboard and Properties File\nOperators and Control Statements\nMethod and Types of methods\nVariable and Types of Variables\nConstructor and Types of constructors\nBlock and Types of Blocks\nDeclarations, Invocations and Executions\nCompiler & JVM Architecture with Reflection API\nStatic Members and their execution control flow\nNon-Static Members and their execution control flow\nFinal Variables and their rules\n','constructor,java,oops,overloading,polymorphysim,inheritance',4),(47,2000,'CDAC','2022-04-05 09:07:40','20','ccat','ds','dac',3),(49,2000,'CDAC','2022-04-05 09:33:44','20','ccat','ds','dac',3),(50,1111,'11111111111111','2022-04-05 09:34:37','1111','1111111111111111','111111111111111','111',5),(52,600,'Advance Java','2022-04-06 06:01:19','30','Core Java','Servelet,Jsp,Hibernate,JPA,Spring Boot','Advance Java',2);
/*!40000 ALTER TABLE `course_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enrollment`
--

DROP TABLE IF EXISTS `enrollment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enrollment` (
  `e_id` int NOT NULL AUTO_INCREMENT,
  `createdtimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_completed` int NOT NULL DEFAULT '0',
  `result` double NOT NULL DEFAULT '0',
  `course_id` int DEFAULT NULL,
  `s_id` int DEFAULT NULL,
  PRIMARY KEY (`e_id`),
  KEY `FK19v0xcq2bk3cja8ajas010qhi` (`course_id`),
  KEY `FKdy0bb7rq4kwcwtokraltrarbk` (`s_id`),
  CONSTRAINT `FK19v0xcq2bk3cja8ajas010qhi` FOREIGN KEY (`course_id`) REFERENCES `course_content` (`course_id`),
  CONSTRAINT `FKdy0bb7rq4kwcwtokraltrarbk` FOREIGN KEY (`s_id`) REFERENCES `student` (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enrollment`
--

LOCK TABLES `enrollment` WRITE;
/*!40000 ALTER TABLE `enrollment` DISABLE KEYS */;
INSERT INTO `enrollment` VALUES (1,'2022-02-07 20:53:12',1,0,4,1),(2,'2022-04-04 09:15:21',1,0,4,3),(3,'2022-04-04 09:15:33',1,0,11,3),(4,'2022-04-06 05:37:32',1,0,11,3),(9,'2022-04-06 06:01:19',0,0,3,5),(10,'2022-04-06 06:01:19',0,0,3,6),(11,'2022-04-06 06:01:19',0,0,4,7),(12,'2022-04-06 06:01:19',0,0,4,8),(15,'2022-04-11 06:34:51',1,2,3,1),(17,'2022-04-18 14:04:52',1,0,11,1);
/*!40000 ALTER TABLE `enrollment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lecturer`
--

DROP TABLE IF EXISTS `lecturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lecturer` (
  `l_id` int NOT NULL AUTO_INCREMENT,
  `createdtimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(255) DEFAULT NULL,
  `experience` int NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `is_active` int NOT NULL DEFAULT '0',
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT 'lecturer',
  `is_deleted` tinyint NOT NULL,
  PRIMARY KEY (`l_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lecturer`
--

LOCK TABLES `lecturer` WRITE;
/*!40000 ALTER TABLE `lecturer` DISABLE KEYS */;
INSERT INTO `lecturer` VALUES (1,'2022-01-29 21:48:32','ashwin8797bharambe@gmail.com',5,'akshay','M',1,'fegade','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','958465874','B-tech','lecturer',0),(2,NULL,'ketan',4,'AKSHAY','Male',0,'FEGADE','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','5454545454','1998-03-03','lecturer',0),(3,'2022-02-14 18:49:13','akshayfegade15@gmail.com',5,'Ashwin','male',0,'Bharambe','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','123456789','BE Mechanical','lecturer',0),(4,'2022-02-14 18:50:22','akshay',5,'Ashwin','male',0,'Bharambe','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','123456789','BE Mechanical','lecturer',0),(5,'2022-03-26 08:33:39','akshay',4,'AKSHAY','Male',0,'FEGADE','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','5454545454','1998-03-03','lecturer',0),(8,'2022-04-06 06:01:19','akshay',5,'Mayur','Male',0,'Bonde','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','1212121212','Electrical Engineer','lecturer',0),(9,'2022-04-06 06:01:19','akshay',5,'Ashwin','Male',0,'Bharambe','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','2323232323','Mechanical Engineer','lecturer',0),(10,'2022-04-06 06:01:19','akshay',5,'Akshay','Male',0,'Fegade','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','3434343434','Chemical Engineer','lecturer',0);
/*!40000 ALTER TABLE `lecturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionbank`
--

DROP TABLE IF EXISTS `questionbank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `questionbank` (
  `q_id` int NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) DEFAULT NULL,
  `createdtimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `option1` varchar(255) DEFAULT NULL,
  `option2` varchar(255) DEFAULT NULL,
  `option3` varchar(255) DEFAULT NULL,
  `option4` varchar(255) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `course_id` int DEFAULT NULL,
  PRIMARY KEY (`q_id`),
  KEY `FKpq9surdqy41wabclexicjelu5` (`course_id`),
  CONSTRAINT `FKpq9surdqy41wabclexicjelu5` FOREIGN KEY (`course_id`) REFERENCES `course_content` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionbank`
--

LOCK TABLES `questionbank` WRITE;
/*!40000 ALTER TABLE `questionbank` DISABLE KEYS */;
INSERT INTO `questionbank` VALUES (1,'4','2022-02-07 20:58:14','1','2','7','4','2+2=?',4),(2,'Give answer here','2022-03-30 14:23:21','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',4),(3,'Give answer here','2022-03-30 14:23:24','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',4),(4,'Give answer here','2022-03-30 14:23:25','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',4),(5,'Give answer here','2022-03-30 14:23:27','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',4),(6,'Give answer here','2022-03-30 14:23:28','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',4),(7,'Give answer here','2022-03-30 14:23:29','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',4),(9,'Give answer here','2022-03-30 14:23:45','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',3),(10,'Give answer here','2022-03-30 14:23:48','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',3),(11,'Give answer here','2022-03-30 14:23:49','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',3),(12,'Give answer here','2022-03-30 14:23:50','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',3),(13,'Give answer here','2022-03-30 14:23:50','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',3),(14,'Give answer here','2022-03-30 14:23:57','Give Option 1 here','Give Option 2 here','Give Option 3 here','Give Option 4 here','Give Question  here',3),(15,'b','2022-04-05 05:46:58','a','b','c','d','what is new question',4),(26,'answer','2022-04-06 18:35:27','option','option','answer','option','This is Question',3),(27,'answer','2022-04-06 18:35:33','option','option','answer','option','This is Question',3),(28,'answer','2022-04-06 18:35:33','option','option','answer','option','This is Question',3),(29,'answer','2022-04-06 18:35:34','option','option','answer','option','This is Question',3),(30,'answer','2022-04-06 18:35:34','option','option','answer','option','This is Question',3),(31,'answer','2022-04-06 18:35:35','option','option','answer','option','This is Question',3),(32,'answer','2022-04-06 18:35:35','option','option','answer','option','This is Question',3),(33,'answer','2022-04-06 18:35:35','option','option','answer','option','This is Question',3),(34,'answer','2022-04-06 18:35:36','option','option','answer','option','This is Question',3),(35,'answer','2022-04-06 18:35:36','option','option','answer','option','This is Question',3),(36,'answer','2022-04-06 18:35:36','option','option','answer','option','This is Question',3),(37,'answer','2022-04-06 18:35:36','option','option','answer','option','This is Question',3),(38,'answer','2022-04-06 18:35:37','option','option','answer','option','This is Question',3),(39,'answer','2022-04-06 18:35:37','option','option','answer','option','This is Question',3),(40,'answer','2022-04-06 18:35:43','option','option','answer','option','This is Question',4),(41,'answer','2022-04-06 18:35:44','option','option','answer','option','This is Question',4),(42,'answer','2022-04-06 18:35:45','option','option','answer','option','This is Question',4),(43,'answer','2022-04-06 18:35:46','option','option','answer','option','This is Question',4),(44,'answer','2022-04-06 18:35:47','option','option','answer','option','This is Question',4),(45,'answer','2022-04-06 18:35:47','option','option','answer','option','This is Question',4),(46,'answer','2022-04-06 18:35:48','option','option','answer','option','This is Question',4),(47,'answer','2022-04-06 18:35:49','option','option','answer','option','This is Question',4),(48,'answer','2022-04-06 18:35:49','option','option','answer','option','This is Question',4),(49,'answer','2022-04-06 18:35:54','option','option','answer','option','This is Question',11),(50,'answer','2022-04-06 18:35:55','option','option','answer','option','This is Question',11),(51,'answer','2022-04-06 18:35:55','option','option','answer','option','This is Question',11),(52,'answer','2022-04-06 18:35:56','option','option','answer','option','This is Question',11),(53,'answer','2022-04-06 18:35:56','option','option','answer','option','This is Question',11),(54,'answer','2022-04-06 18:35:57','option','option','answer','option','This is Question',11),(55,'answer','2022-04-06 18:35:57','option','option','answer','option','This is Question',11),(56,'answer','2022-04-06 18:35:58','option','option','answer','option','This is Question',11),(57,'answer','2022-04-06 18:35:58','option','option','answer','option','This is Question',11),(58,'answer','2022-04-06 18:35:59','option','option','answer','option','This is Question',11),(59,'answer','2022-04-06 18:36:00','option','option','answer','option','This is Question',11),(60,'answer','2022-04-06 18:36:38','option','option','answer','option','This is Question',47),(61,'answer','2022-04-06 18:36:39','option','option','answer','option','This is Question',47),(62,'answer','2022-04-06 18:36:40','option','option','answer','option','This is Question',47),(63,'answer','2022-04-06 18:36:41','option','option','answer','option','This is Question',47),(64,'answer','2022-04-06 18:36:41','option','option','answer','option','This is Question',47),(65,'answer','2022-04-06 18:36:42','option','option','answer','option','This is Question',47),(66,'answer','2022-04-06 18:36:42','option','option','answer','option','This is Question',47),(67,'answer','2022-04-06 18:36:43','option','option','answer','option','This is Question',47),(68,'answer','2022-04-06 18:36:43','option','option','answer','option','This is Question',47),(69,'answer','2022-04-06 18:36:47','option','option','answer','option','This is Question',49),(70,'answer','2022-04-06 18:36:48','option','option','answer','option','This is Question',49),(71,'answer','2022-04-06 18:36:48','option','option','answer','option','This is Question',49),(72,'answer','2022-04-06 18:36:49','option','option','answer','option','This is Question',49),(73,'answer','2022-04-06 18:36:50','option','option','answer','option','This is Question',49),(74,'answer','2022-04-06 18:36:50','option','option','answer','option','This is Question',49),(75,'answer','2022-04-06 18:36:51','option','option','answer','option','This is Question',49),(76,'answer','2022-04-06 18:36:51','option','option','answer','option','This is Question',49),(77,'answer','2022-04-06 18:36:52','option','option','answer','option','This is Question',49),(78,'answer','2022-04-06 18:36:52','option','option','answer','option','This is Question',49),(79,'answer','2022-04-06 18:36:53','option','option','answer','option','This is Question',49),(80,'answer','2022-04-06 18:36:53','option','option','answer','option','This is Question',49),(82,'','2022-04-13 13:07:41','','','','','',47);
/*!40000 ALTER TABLE `questionbank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rating` (
  `rating_id` int NOT NULL AUTO_INCREMENT,
  `rating` decimal(3,2) DEFAULT NULL,
  `course_id` int DEFAULT NULL,
  `s_id` int DEFAULT NULL,
  PRIMARY KEY (`rating_id`),
  KEY `FKkrgry6k09gd6a7mf0lgj6mohu` (`course_id`),
  KEY `FKq1pddihac7cvhscgfwe2r44` (`s_id`),
  CONSTRAINT `FKkrgry6k09gd6a7mf0lgj6mohu` FOREIGN KEY (`course_id`) REFERENCES `course_content` (`course_id`),
  CONSTRAINT `FKq1pddihac7cvhscgfwe2r44` FOREIGN KEY (`s_id`) REFERENCES `student` (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` VALUES (1,3.00,4,1),(2,4.00,4,1),(3,2.00,4,1),(4,7.00,4,1),(5,3.00,3,2),(6,4.00,3,1),(7,2.00,3,2),(23,3.00,4,1),(25,3.00,11,1),(26,3.00,11,1),(27,3.00,11,1),(31,2.50,11,3),(36,4.00,3,5),(37,3.50,3,6),(38,3.80,4,7),(39,3.30,4,8),(42,3.50,4,3);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `review` (
  `review_id` int NOT NULL AUTO_INCREMENT,
  `createdtimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `review` varchar(255) DEFAULT NULL,
  `course_id` int DEFAULT NULL,
  `s_id` int DEFAULT NULL,
  PRIMARY KEY (`review_id`),
  KEY `FK1jbdaoncs1ven30872d9py7y` (`course_id`),
  KEY `FK593g6f7oy9oekx9l60bqhdeb5` (`s_id`),
  CONSTRAINT `FK1jbdaoncs1ven30872d9py7y` FOREIGN KEY (`course_id`) REFERENCES `course_content` (`course_id`),
  CONSTRAINT `FK593g6f7oy9oekx9l60bqhdeb5` FOREIGN KEY (`s_id`) REFERENCES `student` (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,'2022-02-07 20:18:41','Course is value for money',4,1),(3,'2022-04-01 17:19:32','this is riview one',4,1),(4,'2022-04-01 17:19:46','this is riview two',4,2),(5,'2022-04-01 17:19:55','',4,3),(7,'2022-04-01 18:20:40','this is riview three',3,3),(8,'2022-04-01 18:20:47','this is riview three',3,2),(9,'2022-04-06 05:37:57','ashwin',11,3),(14,'2022-04-06 06:01:19','Dot net is amazing.',3,5),(15,'2022-04-06 06:01:19','',3,6),(16,'2022-04-06 06:01:19','Normalization is hard to understand.',4,7),(17,'2022-04-06 06:01:19','MySQL is easy to learn.',4,8),(20,'2022-04-11 06:35:29','good  course',3,1),(22,'2022-04-18 14:05:11','wdedd',11,1);
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student` (
  `s_id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `createdtimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_of_birth` date DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `is_active` int NOT NULL DEFAULT '0',
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `pincode` int NOT NULL,
  `role` varchar(255) DEFAULT 'student',
  PRIMARY KEY (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'E-type near gayatri temple nepanagar (450221)','2022-04-29 04:55:25',NULL,'burhanpur','akshayfegade15@gmail.com','AKSHAY','Male',0,'FEGADE','$2a$10$9wlagloN4MinL.cP6GEtKuZbxXwuh9v/2gWPqYklMbGIV25TdDD6O','',450221,'student'),(2,'Bhusawal',NULL,'1997-12-13','Jalgaon','asb@gmail.com','Ashwin','male',0,'Bharambe','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','123456789',425201,'student'),(3,'E-type near gayatri temple nepanagar (450221)',NULL,'2022-04-05','burhanpur','akshay','akshay','Female',0,'FEGADE','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','3333333333',450221,'student'),(4,'E-type near gayatri temple nepanagar (450221)','2022-04-04 17:17:27',NULL,'burhanpur','akshay@gmail.com','AKSHAY','Male',0,'FEGADE','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','344556',450221,'student'),(5,'Jalgaon','2022-04-04 17:18:36','1995-03-31','Jalgaon','darshan@gmail.com','Darshan','male',0,'Bhangale','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','1234023562',425001,'student'),(6,'Bhusawal','2022-04-06 06:01:19','1997-07-08','Jalgaon','ashwin@gmail.com','Ashwin','Male',0,'Bharambe','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','1212121212',425201,'student'),(7,'Savada','2022-04-06 06:01:19','1997-02-11','Jalgaon','paresh@gmail.com','Paresh','Male',0,'Jawale','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','2323232323',425202,'student'),(8,'Nashirabad','2022-04-06 06:01:19','1997-08-21','Jalgaon','pratik@gmail.com','Pratik','Male',0,'Chaudhary','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','3434343434',425203,'student'),(9,'Asoda','2022-04-06 06:01:19','1997-04-18','Jalgaon','parimal@gmail.com','Parimal','Male',0,'Patil','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','4545454545',425204,'student'),(10,'Bhadali','2022-04-06 06:01:19','1997-07-08','Jalgaon','Khagesh@gmail.com','Khagesh','Male',0,'Mahajan','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','5656565656',425205,'student'),(11,'Pune','2022-04-06 06:01:19','1995-08-14','Pune','devesh@gmail.com','Devesh','Male',0,'Patil','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','6767676767',425206,'student'),(12,'Dombivali','2022-04-06 06:01:19','1991-05-26','Thane','deepak@gmail.com','Deepak','Male',0,'Mahajan','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','7878787878',425207,'student'),(13,'Aurangabad','2022-04-06 06:01:19','1995-06-19','Aurangabad','suraj@gmail.com','Suraj','Male',0,'Bhojane','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','8989898989',425208,'student'),(14,'Thane','2022-04-06 06:01:19','1996-11-28','Thane','rahul@gmail.com','Rahul','Male',0,'Kedare','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','1919191919',425209,'student'),(15,'Manmad','2022-04-06 06:01:19','1992-08-18','Nashik','sachin@gmail.com','Sachin','Male',0,'Joshi','$2a$10$b0t/xpKVJySy3uo/YbsBNuQaev7qjqNzvTLXmmioNB5cdAY2/X3x.','2828282828',425206,'student');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribed`
--

DROP TABLE IF EXISTS `subscribed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subscribed` (
  `sd_id` int NOT NULL AUTO_INCREMENT,
  `createdtimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `l_id` int DEFAULT NULL,
  `sn_id` int DEFAULT NULL,
  PRIMARY KEY (`sd_id`),
  KEY `FKdq7999cirqr7xpco0r66kme78` (`l_id`),
  KEY `FKstjn2vcmb8wt25dxq42w140dx` (`sn_id`),
  CONSTRAINT `FKdq7999cirqr7xpco0r66kme78` FOREIGN KEY (`l_id`) REFERENCES `lecturer` (`l_id`),
  CONSTRAINT `FKstjn2vcmb8wt25dxq42w140dx` FOREIGN KEY (`sn_id`) REFERENCES `subscription` (`sn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribed`
--

LOCK TABLES `subscribed` WRITE;
/*!40000 ALTER TABLE `subscribed` DISABLE KEYS */;
INSERT INTO `subscribed` VALUES (12,'2022-04-14 06:39:18',3,1);
/*!40000 ALTER TABLE `subscribed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subscription` (
  `sn_id` int NOT NULL AUTO_INCREMENT,
  `sn_amount` double NOT NULL,
  `sn_duration` int NOT NULL,
  `sn_plan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription`
--

LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
INSERT INTO `subscription` VALUES (1,300,90,'Silver'),(2,600,180,'Gold'),(8,1200,365,'Platinum');
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `videos` (
  `v_id` int NOT NULL AUTO_INCREMENT,
  `createdtimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `video` varchar(255) DEFAULT NULL,
  `course_id` int DEFAULT NULL,
  `v_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`v_id`),
  KEY `FKe52bjqwycnyvgpuesmlvtoj0u` (`course_id`),
  CONSTRAINT `FKe52bjqwycnyvgpuesmlvtoj0u` FOREIGN KEY (`course_id`) REFERENCES `course_content` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (1,'2022-02-07 19:31:38','https://youtu.be/Tvvt9coRlF4',4,'Jackson - Bidirectional Relationships | @JsonManagedReference, @JsonBackReference | Spring Boot'),(2,'2022-02-07 19:32:28','https://www.youtube.com/watch?v=2IQsjvZoOkg',4,'@PutMapping | Updating the Resource creating api using spring boot | Spring boot tutorial'),(3,'2022-03-24 17:32:22','https://youtu.be/Tvvt9coRlF4',3,'Inheritance'),(4,'2022-03-24 17:32:43','https://youtu.be/Tvvt9coRlF4',3,'Polymorphism'),(5,'2022-03-24 17:32:55','https://youtu.be/Tvvt9coRlF4',3,'Modularity'),(6,'2022-03-24 17:33:17','https://youtu.be/Tvvt9coRlF4',4,'Spring Boot'),(13,'2022-03-25 04:11:58','https://www.youtube.com/watch?v=2IQsjvZoOkg',11,'Inheritance'),(14,'2022-03-25 04:12:09','https://www.youtube.com/watch?v=2IQsjvZoOkg',11,'Modularity'),(15,'2022-03-25 04:12:26','https://www.youtube.com/watch?v=2IQsjvZoOkg',11,'Overloading'),(37,'2022-04-05 09:33:44','youtube/abc/1',49,'videourl1'),(38,'2022-04-05 09:33:44','youtube/abc/2',49,'videourl2'),(39,'2022-04-05 09:34:37','https://www.youtube.com/watch?v=h7huHkvPoEE',50,'video1'),(40,'2022-04-05 09:34:37','https://www.youtube.com/watch?v=h7huHkvPoEE',50,'video2'),(41,'2022-04-05 10:03:49','youtube.com',50,'Video for java'),(52,'2022-04-06 06:01:19','https://www.youtube.com/watch?v=Tp7Coqo2alM',3,'Partial Class'),(53,'2022-04-06 06:01:19','https://www.youtube.com/watch?v=lNjUn1rMqIo',3,'Sealed Class'),(54,'2022-04-06 06:01:19','https://www.youtube.com/watch?v=HsN0mVFpttU',3,'Extension Method'),(55,'2022-04-06 06:01:19','https://www.youtube.com/watch?v=Z1CNOMD8Yuo',3,'Dynamic Keyword'),(56,'2022-04-06 06:01:19','https://www.youtube.com/watch?v=vUj-kUEC_oA',4,'DDL,DML,DCL,TCL Operations'),(57,'2022-04-06 06:01:19','https://www.youtube.com/watch?v=ABwD8IYByfk',4,'Normalization'),(58,'2022-04-06 06:01:19','https://www.youtube.com/watch?v=bLL5NbBEg2I',4,'SQL Joins');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-29 10:58:19
